@test "Check correctness: sort by uid" {
  result="$(fst-sort -k uid data/DJB11.fasta | fst-awk '{print(uid)}' > outputs/djb11_sort_uid.txt)"
  result="$(fst-awk '{print(uid)}' data/DJB11.fasta | sort > outputs/djb11_uid_sort.txt)"
  run diff outputs/djb11_sort_uid.txt outputs/djb11_uid_sort.txt
  [ -z "${lines[0]}" ]
}

@test "Check correctness: sort by ox" {
  result="$(fst-sort -k ox data/DJB11.fasta | fst-awk '{print(ox)}' > outputs/djb11_sort_ox.txt)"
  result="$(fst-awk '{print(ox)}' data/DJB11.fasta | sort -g > outputs/djb11_ox_sort.txt)"
  run diff outputs/djb11_sort_ox.txt outputs/djb11_ox_sort.txt
  [ -z "${lines[0]}" ]
}

@test "Check correctness: reverse sort by uid" {
  result="$(fst-sort -r -k uid data/DJB11.fasta | fst-awk '{print(uid)}' > outputs/djb11_rsort_uid.txt)"
  result="$(fst-awk '{print(uid)}' data/DJB11.fasta | sort -r > outputs/djb11_uid_rsort.txt)"
  run diff outputs/djb11_rsort_uid.txt outputs/djb11_uid_rsort.txt
  [ -z "${lines[0]}" ]
}

@test "Check correctness: reverse sort by ox" {
  result="$(fst-sort -k ox -r data/DJB11.fasta | fst-awk '{print(ox)}' > outputs/djb11_rsort_ox.txt)"
  result="$(fst-awk '{print(ox)}' data/DJB11.fasta | sort -g -r > outputs/djb11_ox_rsort.txt)"
  run diff outputs/djb11_rsort_ox.txt outputs/djb11_ox_rsort.txt
  [ -z "${lines[0]}" ]
}

@test "Check correctness: order to specific order by ox" {
  result="$(fst-sort -k ox -F outputs/djb11_rsort_ox.txt data/DJB11.fasta | fst-awk '{print(ox)}' > outputs/djb11_rsort_ox_reorder.txt)"
  run diff outputs/djb11_rsort_ox.txt outputs/djb11_rsort_ox_reorder.txt
  [ -z "${lines[0]}" ]
}

@test "Check correctness: order to specific order by uid" {
  result="$(fst-sort -k uid -F outputs/djb11_rsort_uid.txt data/DJB11.fasta | fst-awk '{print(uid)}' > outputs/djb11_rsort_uid_reorder.txt)"
  run diff outputs/djb11_rsort_uid.txt outputs/djb11_rsort_uid_reorder.txt
  [ -z "${lines[0]}" ]
}
