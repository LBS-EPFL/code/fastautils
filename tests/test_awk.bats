#!/usr/bin/env bats

@test "Check syntax: just main block" {
  run fst-awk '{print(len(seq.seq))}' data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check syntax: condition + main block" {
  run fst-awk 'ox>0 {print(len(seq.seq))}' data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check syntax: begin + main block" {
  run fst-awk '\BEGIN{a=1} {print(a)}' data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check syntax: begin + condition + main block" {
  run fst-awk '\BEGIN{a=1} a>0 {print(a)}' data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check syntax: begin + main block + end" {
  run fst-awk '\BEGIN{a=1} {print(a)}\END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check syntax: begin + condition + main block + end" {
  run fst-awk '\BEGIN{a=1} a>0 {print(a)}\END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check syntax: block inside block" {
  run fst-awk '\BEGIN{a=1} a>0 {if( a>0 ){print(a)}}\END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check syntax: block inside block inside block" {
  run fst-awk '\BEGIN{a=1} a>0 {if( a>0 ){if a >0 {print(a)}}}\END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check syntax: block inside block inside block and multiline command" {
  run fst-awk '\BEGIN{a=1} a>0 {if( a>0 ){print(a);if a >0 {print(a);print(a)}}}\END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check syntax: begin + condition + end" {
  run fst-awk '\BEGIN{a=1} a>0 \END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check syntax: begin + end" {
  run fst-awk '\BEGIN{a=1} a>0 \END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 0 ]
}

@test "Check syntax: Failure if curly brackets are used improperly 1" {
  run fst-awk '\BEGIN{a=1} a>0 {\END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 1 ]
}

@test "Check syntax: Failure if curly brackets are used improperly 2" {
  run fst-awk '\BEGIN{a=1} a>0 {}}\END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 1 ]
}

@test "Check syntax: Failure if curly brackets are used improperly 3" {
  run fst-awk '\BEGIN{a=1} a>0 {}}\END{print("hi")}}' data/DJB11.fasta
  [ "$status" -eq 1 ]
}

@test "Check syntax: Failure if curly brackets are used improperly 4" {
  run fst-awk '\BEGIN{a{=1} a>0 {}}\END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 1 ]
}

@test "Check syntax: Failure if python syntax error" {
  run fst-awk '\BEGIN{a=1} a>0 {print a}\END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 1 ]
}

@test "Check syntax: Failure if \ is forgotten in front of END" {
  run fst-awk '\BEGIN{a=1} {print(a)}END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 1 ]
}

@test "Check syntax: Failure if \ is forgotten in front of BEGIN" {
  run fst-awk 'BEGIN{a=1} {print(a)}\END{print("hi")}' data/DJB11.fasta
  [ "$status" -eq 1 ]
}

@test "Check correctness: number of outputs" {
  result="$(fst-awk '{print(len(seq.seq))}' data/DJB11.fasta | wc -l)"
  [ "$result" -eq 5 ]
}

@test "Check correctness: length of all sequences" {
  run fst-awk '{print(len(seq.seq))}' data/DJB11.fasta
  [ "${lines[0]}" = 358 ]
  [ "${lines[1]}" = 358 ]
  [ "${lines[2]}" = 358 ]
  [ "${lines[3]}" = 358 ]
  [ "${lines[4]}" = 358 ]
}

@test "Check correctness: input from stdin" {
  result="$(cat data/DJB11.fasta | fst-awk '{print(len(seq.seq))}' | wc -l)"
  [ "$result" -eq 5 ]
}
