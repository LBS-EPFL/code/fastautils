#!/usr/bin/env bats

@test "Check syntax: HPD" {
  run fst-grep "HPD" data/DJB11.fasta -c
  [ "$status" -eq 0 ]
}

@test "Check syntax: gaps" {
  run fst-grep "." data/DJB11.fasta -c
  [ "$status" -eq 0 ]
}

@test "Check syntax: hydrophobic" {
  run fst-grep "@" data/DJB11.fasta -c
  [ "$status" -eq 0 ]
}

@test "Check syntax: polar" {
  run fst-grep "#" data/DJB11.fasta -c
  [ "$status" -eq 0 ]
}

@test "Check syntax: everything" {
  run fst-grep "_" data/DJB11.fasta -c
  [ "$status" -eq 0 ]
}

@test "Check syntax: positive" {
  run fst-grep "+" data/DJB11.fasta -c
  [ "$status" -eq 0 ]
}

@test "Check syntax: negative" {
  run fst-grep "-" data/DJB11.fasta -c
  [ "$status" -eq 0 ]
}

@test "Check syntax: multiple possibilities" {
  run fst-grep "HPD|NPD" data/DJB11.fasta -c
  [ "$status" -eq 0 ]
}

@test "Check correctness: input from stdin" {
  result="$(cat data/DJB11.fasta | fst-grep 'HPD' | wc -l)"
  [ "$result" -eq 10 ]
}

@test "Check correctness: count works as expected" {
  result="$(cat data/DJB11.fasta | fst-grep 'HPD' | wc -l)"
  [ "$result" -eq 10 ]
  result="$(cat data/DJB11.fasta | fst-grep 'HPD' -c)"
  [ "$result" -eq 5 ]
  result="$(cat data/DJB11.fasta | fst-grep 'NPD' | wc -l)"
  [ "$result" -eq 10 ]
  result="$(cat data/DJB11.fasta | fst-grep 'NPD' -c)"
  [ "$result" -eq 5 ]
}

@test "Check correctness: only matching works" {
  run fst-grep 'HPD' data/DJB11.fasta -o
  [ "${lines[1]}" = "HPD" ]
  [ "${lines[3]}" = "HPD" ]
  [ "${lines[5]}" = "HPD" ]
  [ "${lines[7]}" = "HPD" ]
  [ "${lines[9]}" = "HPD" ]
}

@test "Check correctness: '.' only matches gaps " {
  result="$(fst-grep '.' data/DJBX_human.fasta -o | grep -v '>' | sort | uniq )" 
  [ "$result" = "-" ]
}
