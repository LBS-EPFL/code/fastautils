@test "Check correctness: shuf does not modify first columns" {
  result="$(fst-shuf -r 15 data/DJBX_human.fasta | fst-awk '{print(seq.seq[:15])}' > outputs/first15_shuf.txt)"
  result="$(fst-awk '{print(seq.seq[:15])}' data/DJBX_human.fasta  > outputs/first15.txt)"
  run diff outputs/first15.txt outputs/first15_shuf.txt
  [ -z "${lines[0]}" ]
}

@test "Check correctness: the first shuffled column of a region is the correct one" {
  result="$(fst-shuf -r 14 data/DJBX_human.fasta | fst-awk '{print(seq.seq[:15])}' > outputs/first15_14_shuf.txt)"
  result="$(fst-awk '{print(seq.seq[:15])}' data/DJBX_human.fasta  > outputs/first15_14.txt)"
  run diff outputs/first15_14.txt outputs/first15_14_shuf.txt
  [ ! -z "${lines[0]}" ]
}

@test "Check correctness: single shuffled column " {
  result="$(fst-shuf data/DJBX_human.fasta -c 14 | fst-awk '{print(seq.seq[13])}' > outputs/col13_shuf.txt)"
  result="$(fst-awk '{print(seq.seq[13])}' data/DJBX_human.fasta  > outputs/col13.txt)"
  run diff outputs/col13_shuf.txt outputs/col13.txt
  [ ! -z "${lines[0]}" ]
}

@test "Check correctness: shuffle does not change frequencies " {
  result="$(fst-profile data/DJBX_human.fasta > outputs/profile.txt)"
  result="$(fst-shuf -r 15 data/DJBX_human.fasta | fst-profile  > outputs/profile_shuf.txt)"
  run diff outputs/profile_shuf.txt outputs/profile.txt
  [ -z "${lines[0]}" ]
}
